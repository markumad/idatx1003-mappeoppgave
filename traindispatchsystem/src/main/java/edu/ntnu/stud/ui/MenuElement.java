package edu.ntnu.stud.ui;

import java.util.function.UnaryOperator;

/**
 * <h3>Class representing a menu element.</h3>
 * 
 * <p>
 * This class represents a menu element. A menu element has a name and a
 * function that runs when the element is opened. The function takes no
 * arguments and returns void, and is therefore represented by a
 * UnaryOperator.
 * </p>
 *
 * @see Menu
 * @version 1.0.0
 * @since 0.2.0
 */

public class MenuElement {
  private final String name;
  private final UnaryOperator<Void> onOpen;

  /**
   * Constructor for MenuElement.
   *
   * @param name   The name of the menu element
   * @param onOpen The function that runs when the element is opened
   */

  public MenuElement(String name, UnaryOperator<Void> onOpen) {
    this.name = name;
    this.onOpen = onOpen;
  }

  /**
   * Gets the name of the menuElement.
   *
   * @return navn
   */

  public String getName() {
    return name;
  }

  /**
   * Opens the menu element.
   */
  public void open() {
    onOpen.apply(null);
  }
}
