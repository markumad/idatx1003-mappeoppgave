package edu.ntnu.stud.ui;

import edu.ntnu.stud.models.TimeTable;
import edu.ntnu.stud.models.Train;
import edu.ntnu.stud.util.Time;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <h3>Class representing the client for a TrainDispatchSystem.</h3>
 * 
 * <p>
 * This class represents the client for a TrainDispatchSystem. It contains a
 * menu, a train table and the current time. The menu contains menu elements for
 * showing the train table, setting the current time. All menu elements are
 * added in the init() method.
 * </p>
 * 
 * <p>
 * The client can be started with the start() method.
 * </p>
 *
 * @see Menu
 * @see MenuElement
 * @see Train
 * @see TimeTable
 * @see Time
 * @version 1.0.0
 * @since 0.2.0
 */
public class TrainDispatchSystemClient {
  private Menu menu;
  private TimeTable trainTable;
  private Time currentTime;

  /**
   * Initializes the TrainDispatchSystemClient.
   * <p>
   * Creates a new TrainTable, Menu and MenuElements
   * </p>
   */
  public void init() {
    trainTable = new TimeTable();
    currentTime = new Time(0, 0);
    menu = createMainMenu();
  }

  /**
   * Creates the main menu.
   *
   * @return The main menu.
   */
  private Menu createMainMenu() {
    Menu menuTemp = new Menu();

    // Show train table
    menuTemp.addMenuElement(new MenuElement("Show time table", v -> {
      printTable(trainTable);
      return null;
    }));

    // Set current time
    menuTemp.addMenuElement(new MenuElement("Set current time", v -> {
      updateCurrentTime((Time) menu.getValidatedInput(Time.class, "Enter new time (hh:mm): ",
          o -> ((Time) o).compareTo(currentTime) >= 0, "Time must be after current time"));
      return null;
    }));

    // Add train
    menuTemp.addMenuElement(new MenuElement("Add train", v -> {
      System.out.println("Adding train.");

      // Get train number
      final int trainNumber = (Integer) menu.getValidatedInput(
          Integer.class, "Enter train number: ",
          o -> trainTable.idIsValid((Integer) o), "Train number already exists or is invalid");

      // Get line
      final String line = (String) menu.getValidatedInput(String.class, "Enter line: ",
          o -> (String) o != null && !((String) o).isEmpty(), "Line cannot be empty");

      // Get destination
      final String destination = (String) menu.getInput(String.class, "Enter destination: ");

      // Get departure time
      final Time departureTime = (Time) menu.getInput(Time.class, "Enter departure time (hh:mm): ");
      // if departure time is before current time, set day to true.
      // This ensures that the train is within the next 24 hours
      if (departureTime.compareTo(currentTime) < 0) {
        departureTime.setDay(true);
      }

      // Get track
      final int track = (Integer) menu.getValidatedInput(Integer.class, "Enter track: ",
          o -> ((Integer) o > 0) || ((Integer) o == -1),
          "Track must be positive, or -1 to indicate that no track has been set");

      // Get delay
      final Time delay = (Time) menu.getInput(Time.class, "Enter delay (hh:mm): ");

      // Print info about the train to be added
      System.out.println("");
      System.out.println("Train number: " + trainNumber);
      System.out.println("Line: " + line);
      System.out.println("Destination: " + destination);
      System.out.println("Departure time: " + departureTime.getFormattedString());
      System.out.println("Track: " + track);
      System.out.println("Delay: " + delay.getFormattedString());

      // Ask user if the info is correct
      final String input = (String) menu.getInput(String.class, "Is this correct? (y/n): ");

      if (input.equals("y") || input.equals("Y")) {
        trainTable.addTrain(new Train(trainNumber, line, destination, departureTime, track, delay));
        System.out.println("Train added");
        return null;
      }
      System.out.println("Train not added");
      return null;
    }));

    // Search
    menuTemp.addMenuElement(new MenuElement("Search", v -> {
      // Ask user if they want to search by train number or destination
      System.out.println("Search by:");
      System.out.println("1. Train number");
      System.out.println("2. Destination");
      final int choice = (Integer) menu.getValidatedInput(Integer.class, ">",
          o -> ((Integer) o > 0) && ((Integer) o < 3), "Invalid choice");

      // Search by destination
      if (choice == 2) {
        System.out.println("Enter destination to search for");
        final String destination = (String) menu.getInput(String.class, ">");
        final Train[] trains = trainTable.searchByDestination(destination);

        if (trains.length == 0) {
          System.out.println("No trains found");
          return null;
        }

        TimeTable foundTrainsTable = new TimeTable(trains);
        foundTrainsTable.sortTable();
        printTable(foundTrainsTable);
        return null;
      }
      // Search by train number
      System.out.println("Enter train number to search for");
      final int trainNumber = (Integer) menu.getInput(Integer.class, ">");
      final Train train = trainTable.searchById(trainNumber);

      // If train is not found, print message and return
      if (train == null) {
        System.out.println("Departure not found");
        return null;
      }

      // Create a sub menu for the train found
      System.out.println("");

      final Menu subMenu = createTrainSubMenu(train);
      subMenu.start();
      return null;
    }));
    return menuTemp;
  }

  /**
   * Creates a sub menu for a train.
   *
   * @param train The train to create a sub menu for.
   * @return The sub menu.
   */
  private Menu createTrainSubMenu(Train train) {
    final Menu subMenuTemp = new Menu("Train " + train.getTrainNumber(), "Return to main menu");

    // Show train
    subMenuTemp.addMenuElement(new MenuElement("Show train", v1 -> {
      System.out.print(train);
      return null;
    }));

    // Change track
    subMenuTemp.addMenuElement(new MenuElement("Change track", v1 -> {
      int track = (Integer) menu.getValidatedInput(Integer.class, "Enter new track: ",
          o -> ((Integer) o > 0) || ((Integer) o == -1),
          "Track must be positive, or -1 to indicate that no track has been set");
      train.setTrack(track);
      return null;
    }));

    // Change delay
    subMenuTemp.addMenuElement(new MenuElement("Change delay", v1 -> {
      Time delay = (Time) menu.getInput(Time.class, "Enter new delay (hh:mm): ");
      train.setDelay(delay);
      return null;
    }));

    // Change destination
    subMenuTemp.addMenuElement(new MenuElement("Change destination", v1 -> {
      String destination = (String) menu.getInput(String.class, "Enter new destination: ");
      train.setDestination(destination);
      return null;
    }));

    // Delete train
    subMenuTemp.addMenuElement(new MenuElement("Delete train", v1 -> {
      trainTable.removeTrainById(train.getTrainNumber());
      System.out.println("Train deleted");
      subMenuTemp.close();
      return null;
    }));

    // Return the menu created
    return subMenuTemp;
  }

  /**
   * Starts the TrainDispatchSystemClient.
   */

  public void start() {
    menu.start();
  }

  /**
   * Updates the current time.
   *
   * @param newTime The new time.
   */
  private void updateCurrentTime(Time newTime) {
    currentTime = newTime;
    trainTable = new TimeTable(Stream.of(trainTable.getTable())
        .filter(train -> train.getDepartureTime().add(train.getDelay()).compareTo(currentTime) > 0)
        .collect(Collectors.toList()).toArray(new Train[0]));
  }

  /**
   * Prints the train table to the console.
   *
   * @param table The table to be printed.
   */
  private void printTable(TimeTable table) {
    StringBuilder sb = new StringBuilder();
    table.sortTable();
    sb.append(String.format("%-17s %s %n", "",
        "-------- " + currentTime.getFormattedString() + " --------"));
    sb.append(table);
    System.out.println(sb);
  }
}
