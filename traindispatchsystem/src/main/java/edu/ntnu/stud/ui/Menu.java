package edu.ntnu.stud.ui;

import edu.ntnu.stud.util.Formater;
import edu.ntnu.stud.util.Parser;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * <h3>Class representing a navigatable menu.</h3>
 * 
 * <p>
 * This class represents a navigatable menu. It contains a list of menu
 * elements, a title, a boolen for keeping track of if the menu is running and a
 * scanner for getting input from the user.
 * </p>
 * 
 * <p>
 * The menu can be started with the start() method. This method will print the
 * menu and wait for input from the user. The menu will keep running until the
 * close() method is called. The menu will always contain a quit option, and
 * will close when this option is selected.
 * </p>
 * 
 * <p>
 * Menu elements can be added with the addMenuElement() method, which
 * takes a MenuElement as the only argument. The menu element can be opened by
 * calling the openMenuElement() method with the index of the menu element as
 * an argument. This happenes automatically when the user selects the menu
 * element by entering its corresponding index.
 * </p>
 * 
 * <p>
 * The menu class also takes care of getting input from the user. The
 * getInput method will keep asking for input until it gets an input matching a
 * given class. For getting validated input, the getValidatedInput can be used.
 * </p>
 * 
 * <p>
 * The menu class also formats the output to the terminal. It uses the Formater
 * class to move the cursor and clear the line.
 * </p>
 *
 * @see MenuElement
 * @see Parser
 * @version 1.0.0
 * @since 0.2.0
 */

public class Menu {
  private final Scanner scanner = new Scanner(System.in);
  private final ArrayList<MenuElement> menuElements = new ArrayList<>();
  private final String menuTitle;

  private boolean run;

  /**
   * Contructor for Menu.
   * 
   * <p>
   * Creates a new menu with the title "Main menu" and a quit option.
   * </p>
   */

  Menu() {
    setRun(true);
    menuTitle = "Main menu";
    menuElements.add(new MenuElement("Quit", v -> {
      System.out.println("Quitting...");
      close();
      return null;
    }));
  }

  /**
   * Contructor for Menu.
   * 
   * <p>
   * Creates a new menu with the given title and a quit option.
   * </p>
   *
   * @param menuTitle   The title of the menu
   * @param quitMessage The message to be printed when quitting
   */

  Menu(String menuTitle, String quitMessage) {
    setRun(true);
    this.menuTitle = menuTitle;
    menuElements.add(new MenuElement(quitMessage, v -> {
      System.out.println(quitMessage);
      close();
      return null;
    }));
  }

  /**
   * Starts the menu.
   */

  public void start() {
    while (run) {
      printMenu();
      Integer a = (Integer) getInput(Integer.class, ">");
      if (a != null && a < menuElements.size() && a >= 0) {
        openMenuElement(a);
      }
      if (run) {
        System.out.print("Enter to continue");
        scanner.nextLine();
        System.out.println(" ");
      }
    }
  }

  /**
   * Closes the menu.
   */
  public void close() {
    setRun(false);
  }

  /**
   * Sets the run variable.
   *
   * @param run What to set the run variable to
   */
  private void setRun(boolean run) {
    this.run = run;
  }

  /**
   * Method for adding a menu element.
   *
   * @param menuElement The menu element to be added
   */

  public void addMenuElement(MenuElement menuElement) {
    menuElements.add(menuElement);
  }

  /**
   * Method for printing the menu.
   */

  private void printMenu() {
    System.out.println("----" + menuTitle + "----");
    System.out.println("Select an option by entering the corresponding number:");
    for (int i = 0; i < menuElements.size(); i++) {
      System.out.println(i + ": " + menuElements.get(i).getName());
    }
  }

  /**
   * Method for opening a menu element by index.
   *
   * @param index The index of the menu element
   */

  public void openMenuElement(int index) {
    Formater.clearLine();
    System.out.println("");
    menuElements.get(index).open();
  }

  /**
   * Method for getting input from the user.
   *
   * <p>
   * The method will keep asking for input until it gets a valid input.
   * The type param must have a parse function that takes a string as the only
   * argument.
   * </p>
   *
   * @param type    The Class of the input
   * @param message The message to be printed
   * @return Object of type o
   * @throws IllegalArgumentException if Class of the input does not have a parse
   *                                  function with a string as the only argument
   */

  public Object getInput(Class<?> type, String message) throws IllegalArgumentException {
    while (true) {
      // print message
      System.out.print(message);

      // get input from user
      String s = scanner.nextLine();

      try {
        // parse input
        return Parser.parseString(type, s);

      } catch (Exception e) {
        if (e.getClass().equals(IllegalArgumentException.class)) {
          throw new IllegalArgumentException(e.getMessage());
        }

        // show invalid message
        showInvalidMessage("Invalid input.");

      }
    }
  }

  /**
   * Interface for validating input.
   */
  public interface Validator {
    public boolean validate(Object o);
  }

  /**
   * Method for getting validated input from the user.
   * 
   * <p>
   * The method will keep asking for input until it gets a valid input. The type
   * param must have a parse function that takes a string as the only argument.
   * The validator param must implement the Validator interface, found in the
   * Parser class.
   * </p>
   *
   * @param type      The Class of the inputs
   * @param message   The message to be printed
   * @param validator The validator interface to validate the input
   * @return Object of type o
   * 
   * @throws IllegalArgumentException if Class of the input does not have a valid
   *                                  parse function
   * @see Parser
   */

  public Object getValidatedInput(Class<?> type, String message,
      Validator validator, String invalidMessage)
      throws IllegalArgumentException {

    while (true) {
      // get input
      Object input = getInput(type, message);

      // validate input
      if (validator.validate(input)) {

        // clear line and return input
        Formater.clearLine();
        return input;
      }

      // show invalid message
      showInvalidMessage(invalidMessage);
    }
  }

  /**
   * Method for printing and formating invalid input message.
   * 
   * <p>
   * Uses util to move the cursor up and clear the line to allow the user to try
   * again
   * </p>
   *
   * @param message The message to be printed
   */

  private void showInvalidMessage(String message) {
    System.out.print(message);
    Formater.moveCursorToStart();
    Formater.moveCursorUp();
    Formater.clearLine();
  }
}
