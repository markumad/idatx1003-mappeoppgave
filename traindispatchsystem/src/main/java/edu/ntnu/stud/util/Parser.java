package edu.ntnu.stud.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * <h3>Class for parsing.</h3>
 * <p>
 * The parser class contains a method for parsing a string into a specified
 * type. It also contains an interface for validating input.
 * </p>
 *
 * @see Parser#parseString(Class, String)
 * @see Validator
 * @version 1.0.0
 * @since 0.3.0
 * 
 *
 */
public class Parser {

  // Private constructor to prevent instantiation
  private Parser() {
  }

  /**
   * <h3>Method for parsing a String into a specified Class.</h3>
   *
   * <p>
   * The method takes a class as an argument, and returns an object of that type,
   * parsed according to the "default" parse function of that class.
   * This means that the type param must have a parse function that takes a string
   * as the only argument.
   * </p>
   *
   * @param type  The Class to parse the string to
   * @param input The input string to be parsed
   * @return Object of the specified type
   * 
   * @throws InvocationTargetException If the parse function throws an exception
   * @throws IllegalAccessException    If the parse function is not public
   * @throws IllegalArgumentException  If the type does not have a parse function
   *                                   that takes a string as the only argument
   */

  public static Object parseString(Class<?> type, String input)
      throws InvocationTargetException, IllegalAccessException, IllegalArgumentException {

    // The string class dosent have parse function, so we return
    if (type.equals(String.class)) {
      return input;
    }
    // Streams all avaiable methods to the specified class
    Optional<Method> parser = Stream.of(type.getMethods())
        // Filter to find a function that both parses and only takes one String argument
        .filter(m -> m.getName().contains("parse")
            && m.getParameterCount() == 1
            && m.getParameterTypes()[0].equals(String.class))
        // Get the first method
        .findFirst();

    // If a parser is found, invoke it
    if (parser.isPresent()) {
      return parser.get().invoke(null, input);
    }

    // If no parser is found, throw an exception
    throw new IllegalArgumentException(
        "No parser with one String argument found. Implement a parser or use a different type.");
  }
}
