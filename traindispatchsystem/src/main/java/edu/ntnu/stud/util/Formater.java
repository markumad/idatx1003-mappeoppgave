package edu.ntnu.stud.util;

/**
 * Class for formatting the terminal.
 * 
 * <p>
 * This class contains methods for moving the cursor, clearing lines and
 * applying colors to the terminal. The methods are static, so they can be used
 * without instantiating the class.
 * </p>
 *
 * @version 1.0.0
 * @since 0.3.0
 * 
 */
public class Formater {

  // Private constructor to prevent instantiation
  private Formater() {
  }

  /**
   * Method for moving the cursor up or down.
   *
   * @param amount The amount of lines to move the cursor. Positive direction up.
   */
  public static void moveCursor(int amount) {
    System.out.print(String.format("\033[%dA", amount)); // move n lines up or down
  }

  /**
   * Moves the cursor up one line.
   */
  public static void moveCursorUp() {
    moveCursor(1);
  }

  /**
   * Moves the cursor down one line.
   */
  public static void moveCursorDown() {
    moveCursor(-1);
  }

  /**
   * Moves the cursor to the start of the line.
   */
  public static void moveCursorToStart() {
    System.out.print("\033[0G"); // move cursor to start of line
  }

  /**
   * Clears the current line.
   */
  public static void clearLine() {
    System.out.print("\033[2K"); // clear line
  }

}
