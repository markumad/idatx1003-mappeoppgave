package edu.ntnu.stud.util;

/**
 * <h3>Class representing time.</h3>
 * 
 * <p>
 * This class represents time in the format hh:mm, with a day flag to indicate
 * if the time is on the the same day:
 * 
 * <ul>
 * <li>int hour (0-23)</li>
 * <li>int minute (0-59)</li>
 * <li>boolean day (true/false)</li>
 * </ul>
 * This means that the time 25:00 is the same as 01:00 (+1), and that 47:59 is
 * the maximum time that can be represented. If the time exceeds 48 hours, it is
 * capped at 47:59.
 * </p>
 * 
 * <p>
 * The Time class also has methods for adding and comparing times, parsing and
 * and multiple toString methods for different formats.
 * </p>
 *
 * @version 1.0.0
 * @since 0.1.0
 * 
 */
public class Time {
  private int hour; // 0-23
  private int minute; // 0-59
  private boolean day; // true indicates next day

  /**
   * Constructor for Time.
   *
   * @param hour   The hour of the time.
   * @param minute The minute of the time.
   * @throws IllegalArgumentException If any time argument is negative
   */

  public Time(int hour, int minute) throws IllegalArgumentException {
    setHour(hour);
    setMinute(minute);
  }

  /**
   * Constructor for Time with day flag.
   *
   * @param hour   The hour of the time.
   * @param minute The minute of the time.
   * @param day    The day flag.
   * @throws IllegalArgumentException If any time argument is negative
   */

  public Time(int hour, int minute, boolean day) throws IllegalArgumentException {
    setHour(hour);
    setMinute(minute);
    setDay(day);
  }

  /**
   * Returns a string representation of the time object.
   * <p>
   * By default, this does not
   * include the day flag. This means that 25:00 gives "01:00." To include
   * the time flag, use
   * getTotalString() for "25:00" or getFormatted() for "01:00 (+1)".
   * </p>
   */
  public String toString() {
    return String.format("%02d:%02d", hour, minute);
  }

  /**
   * Returns a string representation of the time object.
   * <p>
   * This includes the day flag, such that 25:00 gives "25:00"
   * </p>
   */
  public String getTotalString() {
    return String.format("%02d:%02d", day ? hour + 24 : hour, minute);
  }

  /**
   * Returns a string representation of the time object.
   * <p>
   * This includes the day flag, such that 25:00 gives "01:00 (+1)"
   * </p>
   */
  public String getFormattedString() {
    return String.format("%02d:%02d%s", hour, minute, day ? " (+1)" : "");
  }

  /**
   * Adds a number of minutes to the time object.
   *
   * @param minute The number of minutes to add.
   */

  public void addMinute(int minute) {
    add(0, minute);
  }

  /**
   * Adds a number of hours to the time object.
   *
   * @param hour The number of hours to add.
   */

  public void addHour(int hour) {
    add(hour, 0);
  }

  /**
   * Adds a number of hours and minutes and sets the day flag.
   *
   * <p>
   * Effectively the same as calling add(hour, minute) and setDay(day).
   * </p>
   *
   * @param hour   The number of hours to add.
   * @param minute The number of minutes to add.
   * @param day    The day flag.
   * 
   * @see Time#add(int, int)
   */

  public void add(int hour, int minute, boolean day) {
    if (day) {
      setDay(day);
    }

    add(hour, minute);
  }

  /**
   * Adds a number of hours and minutes to the time object.
   *
   * @param hour   The number of hours to add.
   * @param minute The number of minutes to add.
   * 
   * @throws IllegalArgumentException If any time argument is negative
   */

  public void add(int hour, int minute) throws IllegalArgumentException {
    if (hour < 0 || minute < 0) {
      throw new IllegalArgumentException("Time must be positive");
    }

    // If the total time exceeds 48 hours, cap it
    if (getAbsolute() + (hour * 60) + minute > 48 * 60) {
      setMinute(59);
      setHour(23);
      setDay(true);
      return;
    }

    // add hours
    setHour(this.hour + hour);

    // add minutes
    setMinute(this.minute + minute);
  }

  /**
   * Adds two times together and returns a new time object.
   *
   * @param other The time to add.
   * @return A new time object.
   */

  public Time add(Time other) {
    Time newTime = new Time(this.hour, this.minute, this.day);
    newTime.add(other.getHour(), other.getMinute(), other.isNextDay());
    return newTime;
  }

  public int getAbsolute() {
    return hour * 60 + minute + (day ? 24 * 60 : 0);
  }

  /**
   * Method for comparing two times.
   *
   * @param other The time to compare to.
   * @return 1 if this time is greater than the other, -1 if this time is less, 0
   *         if they are equal.
   */

  public int compareTo(Time other) {

    // compare time flags
    if (day && !other.isNextDay()) {

      return 1;
    } else if (!day && other.isNextDay()) {
      return -1;
    }

    // get absolute time
    int absoluteTime = getAbsolute();
    int otherAbsoluteTime = other.getAbsolute();

    // compare absolute time
    if (absoluteTime > otherAbsoluteTime) {
      return 1;

    } else if (absoluteTime < otherAbsoluteTime) {
      return -1;

    } else {
      return 0;
    }
  }

  public int getHour() {
    return hour;
  }

  /**
   * Sets the hour of the time object.
   *
   * @param hour The hour of the time object.
   * @throws IllegalArgumentException If the hour is negative or the total time is
   *                                  greater than 48 hours.
   */
  public void setHour(int hour) throws IllegalArgumentException {

    // If the hour is negative
    if (hour < 0) {
      throw new IllegalArgumentException("Hour must be positive");
    }

    // If the total time exceeds 48 hours
    if ((hour > 23 && day) || hour > 47) {
      throw new IllegalArgumentException("Time exceeds 48 hours");
    }

    // If the hour is greater than 23, set day to true and subtract 24
    if (hour > 23) {
      day = true;
      hour -= 24;
    }

    // Set hour
    this.hour = hour;
  }

  public int getMinute() {
    return minute;
  }

  /**
   * Sets the minute of the time object.
   * <p>
   * If the minute is greater than 59, the hour is incremented.
   * </p>
   *
   * @param minute The minute of the time object.
   * @throws IllegalArgumentException If the minute is negative
   */

  public void setMinute(int minute) {
    if (minute < 0) {
      throw new IllegalArgumentException("Minute must be positive");
    }

    // add hour if minute is greater than 59
    while (minute > 59) {
      minute -= 60;
      setHour(hour + 1);
    }

    this.minute = minute;
  }

  /**
   * Returns true if the time is on the next day. Effectively the same as a getDay
   * method.
   *
   * @return True if the time is on the next day.
   */
  public boolean isNextDay() {
    return day;
  }

  /**
   * Sets the day flag.
   *
   * @param day The day flag.
   */
  public void setDay(boolean day) {
    this.day = day;
  }

  /**
   * Parses a string to a time object.
   *
   * <p>
   * The string must be in the format hh:mm.
   * </p>
   *
   * @param arg0 The string to parse.
   * @return A new time object.
   * @throws IllegalArgumentException If the string is not formated correctly.
   */

  public static Time parseTime(String arg0) throws IllegalArgumentException {
    // Split string arount :
    String[] time = arg0.split(":");

    if (time.length != 2) {
      throw new IllegalArgumentException("Invalid time format");
    }

    if (time[0].length() > 2 || time[1].length() > 2) {
      throw new IllegalArgumentException("Invalid time format");
    }

    try {
      int hour = Integer.parseInt(time[0]);
      int minute = Integer.parseInt(time[1]);

      if (minute > 59) {
        throw new IllegalArgumentException("Invalid time format");
      }

      if (hour > 23) {
        throw new IllegalArgumentException("Invalid time format");
      }

      return new Time(hour, minute);
    } catch (Exception e) {
      throw new IllegalArgumentException("Invalid time format");
    }
  }

  /**
   * Method for checking if two times are equal.
   *
   * @param o The object to compare to.
   * @return True if the times are equal.
   */
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    }

    if (!(o instanceof Time)) {
      return false;
    }

    Time other = (Time) o;

    return this.hour == other.getHour()
        && this.minute == other.getMinute()
        && this.day == other.isNextDay();
  }
}
