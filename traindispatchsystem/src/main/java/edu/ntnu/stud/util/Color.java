package edu.ntnu.stud.util;

/**
 * Class for storing colors that can be applied to the terminal.
 *
 * <p>
 * The colors are stored as strings, so they can be printed to the terminal.
 * All current colors are:
 * <ul>
 * <li>Colors.NORMAL</li>
 * <li>Colors.GREY</li>
 * </ul>
 * </p>
 *
 * @since 0.4.0
 * @version 1.0.0
 */
public class Color {

  // Private constructor to prevent instantiation
  private Color() {
  }

  public static final String NORMAL = "\033[0m";
  public static final String GREY = "\033[0;30m";
}