package edu.ntnu.stud;

import edu.ntnu.stud.ui.TrainDispatchSystemClient;

/**
 * <h3>Main class.</h3>
 * 
 * <p>
 * This class represents the main class. It contains the main method, which
 * starts the client.
 * </p>
 *
 * @see TrainDispatchSystemClient
 * @version 1.0.0
 * @since 0.1.0
 */
public class Main {
  /**
   * Main method.
   *
   * <p>
   * Initializes and starts the client.
   * </p>
   *
   * @param args Command line arguments. Not used.
   */
  public static void main(String[] args) {
    TrainDispatchSystemClient client = new TrainDispatchSystemClient();
    client.init();
    client.start();
  }
}