package edu.ntnu.stud.models;

import edu.ntnu.stud.util.Color;
import edu.ntnu.stud.util.Time;

/**
 * <h3>Class representing a train departure.</h3>
 * 
 * <p>
 * This class represents a train departure. It contains information about:
 * <ul>
 * <li>Train number (int)</li>
 * <li>Line (String)</li>
 * <li>Destination (String)</li>
 * <li>Departure time (Time)</li>
 * <li>Track (int)</li>
 * <li>Delay (Time)</li>
 * </ul>
 * </p>
 * 
 * <p>
 * The class contains constructors without setting a track or delay. These are
 * to be used when the track or delay is not known. When using these
 * constructors time will by default be set to 00:00, and track will be set to
 * -1.
 * </p>
 * 
 * <p>
 * The class contains getters for all field, but setters only for destination,
 * track and delay. This is because the train number, line and departure time
 * are final. The setters for destination, track and delay throws an exception
 * if the argument is null, or in the case of track, if it is negative and not
 * -1.
 * 
 * </p>
 *
 * @see Time
 * @version 1.0.0
 * @since 0.1.0
 */
public class Train {
  private final int trainNumber;
  private final String line;
  private String destination;
  private final Time departureTime;
  private int track;
  private Time delay;

  /**
   * Constructor for Train.
   *
   * @param trainNumber   The unique train number
   * @param line          The line the train is on
   * @param destination   The destination of the train
   * @param departureTime The departure time of the train
   * @param track         The track the arrives on
   * @param delay         How much the train is delayed
   * 
   * @throws IllegalArgumentException If the train number is negative, the track
   *                                  is negative and not -1, and if any arguments
   *                                  are null.
   */

  public Train(int trainNumber, String line, String destination,
      Time departureTime, int track, Time delay)
      throws IllegalArgumentException {

    if (line == null || destination == null || departureTime == null) {
      throw new IllegalArgumentException("Arguments cannot be null");
    }

    if (trainNumber <= 0) {
      throw new IllegalArgumentException("Train number must be positive");
    }

    this.trainNumber = trainNumber;
    this.line = line;
    setDestination(destination);
    this.departureTime = departureTime;
    setTrack(track);
    setDelay(delay);
  }

  /**
   * Constructor for Train without setting a track.
   *
   * @param trainNumber   The unique train number
   * @param line          The line the train is on
   * @param destination   The destination of the train
   * @param departureTime The departure time of the train
   * @param delay         How much the train is delayed
   * 
   * @throws IllegalArgumentException If the train number is negative or any
   *                                  arguments are null.
   */

  public Train(int trainNumber, String line, String destination, Time departureTime, Time delay)
      throws IllegalArgumentException {
    if (line == null || destination == null || departureTime == null) {
      throw new IllegalArgumentException("Arguments cannot be null");
    }

    if (trainNumber <= 0) {
      throw new IllegalArgumentException("Train number must be positive");
    }

    this.trainNumber = trainNumber;
    this.line = line;
    setDestination(destination);
    this.departureTime = departureTime;
    setTrack(-1);
    setDelay(delay);
  }

  /**
   * Constructor for Train without setting a delay.
   *
   * @param trainNumber   The unique train number
   * @param line          The line the train is on
   * @param destination   The destination of the train
   * @param departureTime The departure time of the train
   * @param track         The track the arrives on
   * 
   * @throws IllegalArgumentException If the train number is negative, the track
   *                                  is negative and not -1 or any arguments are
   *                                  null.
   */
  public Train(int trainNumber, String line, String destination, Time departureTime, int track)
      throws IllegalArgumentException {
    if (line == null || destination == null || departureTime == null) {
      throw new IllegalArgumentException("Arguments cannot be null");
    }

    if (trainNumber <= 0) {
      throw new IllegalArgumentException("Train number must be positive");
    }

    this.trainNumber = trainNumber;
    this.line = line;
    setDestination(destination);
    this.departureTime = departureTime;
    setTrack(track);
    setDelay(new Time(0, 0));
  }

  /**
   * Constructor for Train without setting a track or delay.
   *
   * @param trainNumber   The unique train number
   * @param line          The line the train is on
   * @param destination   The destination of the train
   * @param departureTime The departure time of the train
   * 
   * @throws IllegalArgumentException If the train number is negative or any
   *                                  arguments are null.
   */

  public Train(int trainNumber, String line, String destination, Time departureTime)
      throws IllegalArgumentException {
    if (line == null || destination == null || departureTime == null) {
      throw new IllegalArgumentException("Arguments cannot be null");
    }

    if (trainNumber <= 0) {
      throw new IllegalArgumentException("Train number must be positive");
    }

    this.trainNumber = trainNumber;
    this.line = line;
    setDestination(destination);
    this.departureTime = departureTime;
    setTrack(-1);
    setDelay(new Time(0, 0));
  }

  public int getTrainNumber() {
    return trainNumber;
  }

  public String getLine() {
    return line;
  }

  public String getDestination() {
    return destination;
  }

  /**
   * Sets the destination of the train.
   *
   * @param destination The destination of the train
   * @throws IllegalArgumentException If the destination is null
   */

  public void setDestination(String destination) throws IllegalArgumentException {
    if (destination == null) {
      throw new IllegalArgumentException("Destination cannot be null");
    }
    this.destination = destination;
  }

  public int getTrack() {
    return track;
  }

  /**
   * Sets the track of the train.
   *
   * @param track The track of the train
   * @throws IllegalArgumentException If the track is negative and not -1
   */

  public void setTrack(int track) throws IllegalArgumentException {
    if (track <= 0 && track != -1) {
      throw new IllegalArgumentException("Track must be positive or -1");
    }
    this.track = track;
  }

  public Time getDepartureTime() {
    return departureTime;
  }

  public Time getDelay() {
    return delay;
  }

  /**
   * Sets the delay of the train.
   *
   * @param delay The delay of the train
   * @throws IllegalArgumentException If the delay is null
   */
  public void setDelay(Time delay) throws IllegalArgumentException {
    if (delay == null) {
      throw new IllegalArgumentException("Delay cannot be null");
    }
    this.delay = delay;
  }

  /**
   * Returns a formated string representation of the train.
   * <p>
   * This includes the departure time, line, destination, track and train number.
   * </p>
   *
   * @return A formated string representation of the train
   */
  public String toString() {
    return (String.format(
        "%-20s %-5s %-15s %-15s %-10s %n",
        (delay.getMinute() > 0 || delay.getHour() > 0
            ? Color.GREY + departureTime + Color.NORMAL + " "
                + departureTime.add(delay) + " +" + delay.getTotalString() + "  "
            : departureTime),
        line.length() > 5
            ? line.substring(0, 3) + "..."
            : line,
        destination.length() > 15
            ? destination.substring(0, 12) + "..."
            : destination,
        track == -1
            ? ""
            : "Track: " + track,
        trainNumber));
  }
}
