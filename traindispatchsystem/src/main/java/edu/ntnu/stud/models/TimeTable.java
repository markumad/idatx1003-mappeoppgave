package edu.ntnu.stud.models;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <h3>Class representing a table of train departures.</h3>
 * 
 * <p>
 * This class represents a table of train departures through an ArrayList.
 * </p>
 * 
 * <p>
 * Trains can be added to the table through the addTrain method. This method
 * takes a train as an argument. If the train is null, or the id is invalid, an
 * InvalidArgumentException is thrown. The id is invalid if it is negative or
 * already in use. Trains can be removed from the table by id through the
 * removeTrainById() function. If a train with the given id is not found,
 * nothing happens.
 * </p>
 * 
 * <p>
 * The table can be searched with two different methods. The searchById() method
 * takes an id as the only argument, and returns the corresponding train if it
 * exists, or null if it does not.
 * The searchByDestination() method takes a destination as the only argument,
 * and returns an array of trains with the given destination. If no trains are
 * found, an empty Train array is returned.
 * </p>
 * 
 * <p>
 * The table can be sorted by departure time through the sortTable() method.
 * </p>
 * 
 *
 * @see Train
 * @version 1.0.0
 * @since 0.1.0
 */
public class TimeTable {
  private final ArrayList<Train> table = new ArrayList<>();

  /**
   * Constructor for TimeTable that takes no arguments.
   */
  public TimeTable() {
  }

  /**
   * Constructor for TimeTable, that takes a train.
   *
   * @param train The train to add to the table
   */
  public TimeTable(Train train) {
    addTrain(train);
  }

  /**
   * Constructor for TimeTable, that takes an array of trains.
   *
   * @param trains The trains to add to the table
   */
  public TimeTable(Train[] trains) {
    for (Train train : trains) {
      addTrain(train);
    }
  }

  /**
   * Returns the table as an array.
   *
   * @return The table as an array
   */

  public Train[] getTable() {
    return table.toArray(new Train[0]);
  }

  /**
   * Adds a train to the table.
   *
   * @param train The train to add
   * @throws IllegalArgumentException If the train is null or the id is invalid
   */
  public void addTrain(Train train) throws IllegalArgumentException {
    if (train == null) {
      throw new IllegalArgumentException("Train cannot be null");
    }
    if (!idIsValid(train.getTrainNumber())) {
      throw new IllegalArgumentException("Train number already exists");
    }

    table.add(train);
  }

  /**
   * Removes a train from the table by a given id.
   * 
   * <p>
   * If a train with the given id is not found, nothing happens.
   * </p>
   *
   * @param id The id of the train to remove
   */
  public void removeTrainById(int id) {
    table.removeIf(train -> train.getTrainNumber() == id);
  }

  /**
   * Searches for a train by id.
   *
   * @param id The id of the train
   * @return The train if found, null if not found
   */
  public Train searchById(int id) {
    return table.stream().filter(train -> train.getTrainNumber() == id).findFirst().orElse(null);
  }

  /**
   * Searches for trains by destination.
   *
   * @param destination The destination of the train
   * @return An array of trains with the given destination
   */

  public Train[] searchByDestination(String destination) {
    return table.stream().filter(train -> train.getDestination().equals(destination))
        .collect(Collectors.toList()).toArray(new Train[0]);
  }

  /**
   * Checks if a gived train id is valid (not already in use).
   *
   * @param id The id to check
   * @return True if the id is valid, false if not
   */

  public boolean idIsValid(int id) {
    return table.stream().noneMatch(train -> train.getTrainNumber() == id) && id > 0;
  }

  /**
   * Sorts the table by departure time.
   */
  public void sortTable() {
    table.sort((train1, train2) -> train1.getDepartureTime().compareTo(train2.getDepartureTime()));
  }

  /**
   * Returns the table as a formated string.
   *
   * @return The table as a string
   */

  public String toString() {
    StringBuilder sb = new StringBuilder();

    sb.append(String.format("%-12s %-7s %-5s %-15s %-15s %-10s %n", "Time", "Delay", "Line",
        "Destination", "Track", "Id"));
    sb.append(
        Stream.of(getTable())
            .map(Train::toString)
            .collect(Collectors.joining("")));
    return sb.toString();
  }
}
