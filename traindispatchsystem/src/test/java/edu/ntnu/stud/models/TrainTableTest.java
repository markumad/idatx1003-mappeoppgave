package edu.ntnu.stud.models;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

import edu.ntnu.stud.util.Time;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for TrainTable.
 */
class TrainTableTest {
  TimeTable trainTable;
  Train train;

  @BeforeEach
  void setup() {
    trainTable = new TimeTable();
    train = new Train(1, "line", "destination", new Time(10, 20), 1);
  }

  @Test
  void testGetTable() {
    trainTable = new TimeTable(train);
    Train[] testTable = new Train[1];
    testTable[0] = train;
    assertArrayEquals(testTable, trainTable.getTable());
  }

  @Test
  void testGetTableEmpty() {
    Train[] testTable = new Train[0];
    assertArrayEquals(testTable, trainTable.getTable());
  }

  @Test
  void testAddTrain() {
    trainTable.addTrain(train);
    Train[] testTable = new Train[1];
    testTable[0] = train;
    assertArrayEquals(testTable, trainTable.getTable());
  }

  @Test
  void addMultipleTrain() {
    Train train2 = new Train(2, "line", "destination", new Time(10, 20), 1, new Time(10, 20));
    trainTable.addTrain(train);
    trainTable.addTrain(train2);

    Train[] testTable = new Train[2];
    testTable[0] = train;
    testTable[1] = train2;
    assertArrayEquals(testTable, trainTable.getTable());
  }

  @Test
  void addNullTrain() {
    assertThrowsExactly(IllegalArgumentException.class, () -> trainTable.addTrain(null));
  }

  @Test
  void addTrainWithInvalidId() {
    trainTable.addTrain(train);
    // adding the same train twice means the id is already used, and therefore
    // invalid
    assertThrowsExactly(IllegalArgumentException.class, () -> trainTable.addTrain(train));
  }

  @Test
  void testIdIsValid() {
    trainTable.addTrain(train);
    assertEquals(true, trainTable.idIsValid(2));
  }

  @Test
  void testIdIsValidInEmptyTable() {
    assertEquals(true, trainTable.idIsValid(1));
  }

  @Test
  void testIdIsValidWithInvalidId() {
    trainTable.addTrain(train);
    assertEquals(false, trainTable.idIsValid(1));
  }

  @Test
  void testRemoveTrainById() {
    trainTable.addTrain(train);
    trainTable.removeTrainById(1);
    Train[] testTable = new Train[0];
    assertArrayEquals(testTable, trainTable.getTable());
  }

  @Test
  void testSearchByDestination() {
    trainTable.addTrain(train);
    Train train2 = new Train(2, "line", "destination", new Time(10, 20), 1, new Time(10, 20));
    trainTable.addTrain(train2);
    trainTable.addTrain(new Train(3,
        "line2", "destination2", new Time(10, 20), 1, new Time(10, 20)));

    Train[] testTable = new Train[2];
    testTable[0] = train;
    testTable[1] = train2;

    assertArrayEquals(testTable, trainTable.searchByDestination("destination"));
  }

  @Test
  void testSearchByDestinationEmptyTable() {
    assertArrayEquals(new Train[0], trainTable.searchByDestination("destination"));
  }

  @Test
  void testSearchByDestinationNoMatch() {
    trainTable.addTrain(train);
    assertArrayEquals(new Train[0], trainTable.searchByDestination("destination2"));
  }

  @Test
  void testSearchById() {
    trainTable.addTrain(train);
    assertEquals(train, trainTable.searchById(1));
  }

  @Test
  void testSearchByIdMultipleTrains() {
    trainTable.addTrain(train);
    Train train2 = new Train(2, "line", "destination", new Time(10, 20), 1, new Time(10, 20));
    trainTable.addTrain(train2);
    trainTable.addTrain(new Train(3,
        "line2", "destination2", new Time(10, 20), 1, new Time(10, 20)));
    assertEquals(train2, trainTable.searchById(2));
  }

  @Test
  void testSearchByIdEmptyTable() {
    assertEquals(null, trainTable.searchById(1));
  }

  @Test
  void searchByIdNoMatch() {
    trainTable.addTrain(train);
    assertEquals(null, trainTable.searchById(2));
  }

  @Test
  void testSortTable() {
    Train train2 = new Train(2, "line", "destination", new Time(14, 00), 1);
    Train train3 = new Train(3, "line", "destination", new Time(15, 20), 1);
    trainTable.addTrain(train3);
    trainTable.addTrain(train);
    trainTable.addTrain(train2);

    Train[] testTable = new Train[3];
    testTable[0] = train;
    testTable[1] = train2;
    testTable[2] = train3;

    trainTable.sortTable();
    assertArrayEquals(testTable, trainTable.getTable());

  }

  @Test
  void testSortTableWithDelay() {
    Train train2 = new Train(2, "line", "destination", new Time(14, 00), 1, new Time(00, 00));
    Train train3 = new Train(3, "line", "destination", new Time(8, 00), 1, new Time(10, 20));
    trainTable.addTrain(train3);
    trainTable.addTrain(train);
    trainTable.addTrain(train2);

    Train[] testTable = new Train[3];

    // even though train3 is added last, it should be first in the table because
    // delay has no impact on sorting
    testTable[0] = train3;
    testTable[1] = train;
    testTable[2] = train2;

    trainTable.sortTable();
    assertArrayEquals(testTable, trainTable.getTable());
  }

  @Test
  void testSortTableEmpty() {
    trainTable.sortTable();
    assertArrayEquals(new Train[0], trainTable.getTable());
  }

  @Test
  void testToString() {
    trainTable.addTrain(train);
    String tableHeader = "Time         Delay   Line  Destination     Track           Id         \n";
    assertEquals(tableHeader + train.toString(), trainTable.toString());
  }

  @Test
  void testToStringMultipleTrains() {
    final String tableHeader = "Time         Delay   Line  Destination"
        + "     Track           Id         \n";
    Train train2 = new Train(2, "line", "destination", new Time(14, 00), 1, new Time(00, 00));
    Train train3 = new Train(3, "line", "destination", new Time(8, 00), 1, new Time(10, 20));
    trainTable.addTrain(train);
    trainTable.addTrain(train2);
    trainTable.addTrain(train3);

    assertEquals(tableHeader + train.toString() + train2.toString() + train3.toString(),
        trainTable.toString());
  }
}
