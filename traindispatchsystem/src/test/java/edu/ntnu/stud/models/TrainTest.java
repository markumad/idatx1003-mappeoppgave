package edu.ntnu.stud.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

import edu.ntnu.stud.util.Time;
import org.junit.jupiter.api.Test;

/**
 * Test class for Train.
 */
class TrainTest {

  @Test
  void testConstructor() {
    Train train = new Train(1, "line", "destination", new Time(10, 20), 1, new Time(10, 20));
    assertEquals(1, train.getTrainNumber());
    assertEquals("line", train.getLine());
    assertEquals("destination", train.getDestination());
    assertEquals(1, train.getTrack());
    assertEquals(new Time(10, 20), train.getDepartureTime());
    assertEquals(new Time(10, 20), train.getDelay());
  }

  @Test
  void testConstructorWithoutTrack() {
    Train train = new Train(1, "line", "destination", new Time(10, 20), new Time(10, 20));
    assertEquals(1, train.getTrainNumber());
    assertEquals("line", train.getLine());
    assertEquals("destination", train.getDestination());
    assertEquals(-1, train.getTrack());
    assertEquals(new Time(10, 20), train.getDepartureTime());
    assertEquals(new Time(10, 20), train.getDelay());
  }

  @Test
  void testConstructorWithoutDelay() {
    Train train = new Train(1, "line", "destination", new Time(10, 20), 1);
    assertEquals(1, train.getTrainNumber());
    assertEquals("line", train.getLine());
    assertEquals("destination", train.getDestination());
    assertEquals(1, train.getTrack());
    assertEquals(new Time(10, 20), train.getDepartureTime());
    assertEquals(new Time(0, 0), train.getDelay());
  }

  @Test
  void testConstructorWithoutTrackAndDelay() {
    Train train = new Train(1, "line", "destination", new Time(10, 20));
    assertEquals(1, train.getTrainNumber());
    assertEquals("line", train.getLine());
    assertEquals("destination", train.getDestination());
    assertEquals(-1, train.getTrack());
    assertEquals(new Time(10, 20), train.getDepartureTime());
    assertEquals(new Time(0, 0), train.getDelay());
  }

  @Test
  void testConstructorWithNullLine() {
    assertThrowsExactly(IllegalArgumentException.class,
        () -> new Train(1, null, "destination", new Time(10, 20), 1, new Time(10, 20)));
  }

  @Test
  void testConstructorNullDestination() {
    assertThrowsExactly(IllegalArgumentException.class,
        () -> new Train(1, "line", null, new Time(10, 20), 1, new Time(10, 20)));
  }

  @Test
  void testConstructorNullDepartureTime() {
    assertThrowsExactly(IllegalArgumentException.class,
        () -> new Train(1, "line", "destination", null, 1, new Time(10, 20)));
  }

  @Test
  void testConstructorNullDelay() {
    assertThrowsExactly(IllegalArgumentException.class,
        () -> new Train(1, "line", "destination", new Time(10, 20), 1, null));
  }

  @Test
  void testConstructorWithIllegalTrack() {
    assertThrowsExactly(IllegalArgumentException.class,
        () -> new Train(1, "line", "destination", new Time(10, 20), 0, new Time(10, 20)));
    assertThrowsExactly(IllegalArgumentException.class,
        () -> new Train(1, "line", "destination", new Time(10, 20), -2, new Time(10, 20)));
    assertThrowsExactly(IllegalArgumentException.class,
        () -> new Train(1, "line", "destination", new Time(10, 20), 0));
    assertThrowsExactly(IllegalArgumentException.class,
        () -> new Train(1, "line", "destination", new Time(10, 20), -2));
  }

  void testSetDelay() {
    Train train = new Train(1, "line", "destination", new Time(10, 20), 1, new Time(0, 0));
    train.setDelay(new Time(10, 20));
    assertEquals(new Time(10, 20), train.getDelay());
  }

  @Test
  void testSetDelayNull() {
    Train train = new Train(1, "line", "destination", new Time(10, 20), 1, new Time(0, 0));
    assertThrowsExactly(IllegalArgumentException.class,
        () -> train.setDelay(null));
  }

  @Test
  void testSetDestination() {
    Train train = new Train(1, "line", "destination", new Time(10, 20), 1, new Time(0, 0));
    train.setDestination("new destination");
    assertEquals("new destination", train.getDestination());
  }

  @Test
  void testSetDestinationNull() {
    Train train = new Train(1, "line", "destination", new Time(10, 20), 1, new Time(0, 0));
    assertThrowsExactly(IllegalArgumentException.class,
        () -> train.setDestination(null));
  }

  @Test
  void testSetTrack() {
    Train train = new Train(1, "line", "destination", new Time(10, 20), 1, new Time(0, 0));
    train.setTrack(2);
    assertEquals(2, train.getTrack());
  }

  @Test
  void testToString() {
    Train train = new Train(1, "line", "destination", new Time(10, 20), 1, new Time(0, 0));
    String expected = "10:20                line  destination     Track: 1        1          \n";
    assertEquals(expected, train.toString());
  }

  @Test
  void testToStringWithDelay() {
    Train train = new Train(1, "line", "destination", new Time(10, 20), 1, new Time(10, 20));
    String expected = "[0;30m10:20[0m 20:40 +10:20   line  destination"
        + "     Track: 1        1          \n";
    assertEquals(expected, train.toString());
  }

  @Test
  void testToStringWithNoTrack() {
    Train train = new Train(1, "line", "destination", new Time(10, 20), new Time(0, 0));
    String expected = "10:20                line  destination                     1          \n";
    assertEquals(expected, train.toString());
  }

  @Test
  void testToStringWithNoTrackDelay() {
    Train train = new Train(1, "line", "destination", new Time(10, 20), new Time(10, 20));
    String expected = "[0;30m10:20[0m 20:40 +10:20   line  destination"
        + "                     1          \n";
    assertEquals(expected, train.toString());
  }
}
