package edu.ntnu.stud.util;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

import java.lang.reflect.InvocationTargetException;
import org.junit.jupiter.api.Test;

/**
 * This class contains unit tests for the Parser class.
 */
class ParserTest {

  /**
   * Test string parsing.
   */
  @Test
  void testParseString() {
    assertDoesNotThrow(() -> {
      String string;
      string = (String) Parser.parseString(String.class, "test");
      assertEquals("test", string);
    });
  }

  /**
   * Test integer parsing.
   */
  @Test
  void testParseInteger() {
    assertDoesNotThrow(() -> {
      Integer integer;
      integer = (Integer) Parser.parseString(Integer.class, "102938");
      assertEquals(102938, integer);
    });
  }

  /**
   * Test unparsable integer.
   */
  @Test
  void testParseUnparsableInteger() {
    assertThrowsExactly(InvocationTargetException.class, () -> {
      Parser.parseString(Integer.class, "a");
    });
  }

  /**
   * Test time parsing.
   */
  @Test
  void testParseTime() {
    assertDoesNotThrow(() -> {
      Time time;
      time = (Time) Parser.parseString(Time.class, "23:10");
      assertEquals(new Time(23, 10), time);
    });
  }

  /**
   * Test unparsable time.
   */
  @Test
  void testParseUnparsableTime() {
    assertThrowsExactly(InvocationTargetException.class, () -> {
      Parser.parseString(Time.class, "12931");
    });
  }

  /**
   * Test parsing of a class without a parse function.
   */
  @Test
  void testParseClassWithoutParseFunction() {
    assertThrowsExactly(IllegalArgumentException.class, () -> {
      Parser.parseString(Parser.class, "test");
    });
  }
}
