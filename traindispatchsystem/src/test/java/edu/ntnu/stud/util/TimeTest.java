package edu.ntnu.stud.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * This class contains unit tests for the Time class.
 */
class TimeTest {
  Time time;

  @BeforeEach
  void setup() {
    time = new Time(0, 0);
  }

  @Test
  void testSetMinute() {
    // test setMinute
    time.setMinute(10);
    assertEquals(new Time(0, 10), time);
  }

  @Test
  void testSetMinuteOverflow() {
    // test setMinute with overflow
    time.setMinute(130);
    assertEquals(new Time(2, 10), time);
  }

  @Test
  void testSetMinuteOverflowDay() {
    // test setMinute with day overflow
    time.setMinute(1440);
    assertEquals(new Time(0, 0, true), time);
  }

  @Test
  void testSetMinuteNegative() {
    // test setMinute with negative argument
    assertThrows(IllegalArgumentException.class, () -> time.setMinute(-61));

    // make sure the time is unchanged
    assertEquals(new Time(0, 0), time);
  }

  @Test
  void testSetHour() {
    // test setHour
    time.setHour(10);
    assertEquals(new Time(10, 0), time);
  }

  @Test
  void testSetHourOverflow() {
    // test with overflow
    time.setHour(25);
    assertEquals(new Time(1, 0, true), time);
  }

  @Test
  void testSetHourNegative() {
    // test setHour
    assertThrows(IllegalArgumentException.class, () -> time.setHour(-1));

    // make sure the time is unchanged
    assertEquals(new Time(0, 0), time);
  }

  @Test
  void testSetDay() {
    // test setDay
    time.setDay(true);
    assertEquals(new Time(0, 0, true), time);
  }

  @Test
  void testCompareToEqual() {
    // compare to a time with the same time
    assertEquals(0, time.compareTo(new Time(0, 0)));
    assertEquals(0, new Time(23, 1, true).compareTo(new Time(23, 1, true)));
  }

  @Test
  void testCompareLess() {
    // compare to a time with a greater time
    assertEquals(-1, time.compareTo(new Time(0, 1)));
  }

  @Test
  void testCompareLessDayFlag() {
    // compare to a time with a positive day flag
    assertEquals(-1, new Time(20, 30).compareTo(new Time(0, 0, true)));
  }

  @Test
  void testCompareToGreater() {
    assertEquals(-1, time.compareTo(new Time(1, 0)));
  }

  @Test
  void testCompareToGreaterDayFlag() {
    assertEquals(1, new Time(0, 0, true).compareTo(new Time(20, 30)));
  }

  @Test
  void testSimpleAdd() {
    // simple add
    time.add(1, 1);
    assertEquals(time, new Time(1, 1));
  }

  @Test
  void testSeparateAdd() {
    // separate add
    time.addMinute(1);
    time.addHour(1);
    assertEquals(time, new Time(1, 1));
  }

  @Test
  void testSeparateMaximumAdd() {
    // separate maximum add
    time.addMinute(100000);
    time.addHour(100000);
    assertEquals(new Time(23, 59, true), time);
  }

  @Test
  void testAddOverflow() {
    // add with overflow
    time.add(23, 150);
    assertEquals(new Time(1, 30, true), time);
  }

  @Test
  void testMaximalAdd() {
    // add time such as it caps at 47:59
    time.add(1, 59, true);
    time.add(23, 59);
    assertEquals(new Time(23, 59, true), time);
  }

  @Test
  void testAddNegativeMinute() {
    // adding any negative time should throw an exception
    time.add(10, 10);
    assertThrows(IllegalArgumentException.class, () -> time.add(0, -1));
    // make sure the time is unchanged
    assertEquals(time, new Time(10, 10));
  }

  @Test
  void testAddNegativeHour() {
    // adding any negative time should throw an exception
    time.add(10, 10);
    assertThrows(IllegalArgumentException.class, () -> time.add(-1, 0));
    // make sure the time is unchanged
    assertEquals(new Time(10, 10), time);
  }

  @Test
  void testAddNegativeCombined() {
    // adding any negative time should throw an exception
    time.add(10, 10);
    assertThrows(IllegalArgumentException.class, () -> time.add(-1, -1));
    // make sure the time is unchanged
    assertEquals(new Time(10, 10), time);
  }

  @Test
  void testGetAbsolute() {
    assertEquals(61, new Time(1, 1).getAbsolute());
  }

  @Test
  void testGetAbsoluteMin() {
    assertEquals(0, new Time(0, 0).getAbsolute());
  }

  @Test
  void testGetAbsoluteDay() {
    assertEquals(24 * 60, new Time(0, 0, true).getAbsolute());
  }

  @Test
  void testGetAbsoluteMax() {
    assertEquals(24 * 60 + 61, new Time(1, 1, true).getAbsolute());
  }

  @Test
  void testGetAbsoluteOverflow() {
    // test absolute time with overflow
    assertEquals(24 * 60 + 61, new Time(25, 1).getAbsolute());
  }

  @Test
  void testToString() {
    // test toString
    assertEquals("01:01", new Time(1, 1).toString());
  }

  @Test
  void testToStringMin() {
    assertEquals("00:00", time.toString());
  }

  @Test
  void testToStringMax() {
    assertEquals("23:59", new Time(23, 59, true).toString());
  }

  @Test
  void testToStringDay() {
    assertEquals("01:01", new Time(1, 1, true).toString());
  }

  // test getFormattedString
  @Test
  void testGetFormated() {
    assertEquals("01:01", new Time(1, 1).getFormattedString());
  }

  @Test
  void testGetFormatedMin() {
    assertEquals("00:00", time.getFormattedString());
  }

  @Test
  void testGetFormatedMax() {
    assertEquals("23:59 (+1)", new Time(23, 59, true).getFormattedString());
  }

  @Test
  void testGetFormatedDay() {
    assertEquals("00:00 (+1)", new Time(0, 0, true).getFormattedString());
  }

  // test getTotalString
  @Test
  void testGetTotalString() {
    assertEquals("01:01", new Time(1, 1).getTotalString());
  }

  @Test
  void testGetTotalStringMin() {
    assertEquals("00:00", time.getTotalString());
  }

  @Test
  void testGetTotalStringMax() {
    assertEquals("47:59", new Time(23, 59, true).getTotalString());
  }

  @Test
  void testGetTotalStringDay() {
    assertEquals("25:01", new Time(1, 1, true).getTotalString());
  }

  @Test
  void testParse() {
    // test parse
    Time time = Time.parseTime("12:00");
    assertEquals(new Time(12, 0), time);
  }

  @Test
  // test parse with illegal argument
  void testParseIllegalArgument() {
    assertThrowsExactly(IllegalArgumentException.class, () -> {
      Time.parseTime("a");
    });
  }

  @Test
  void testParseIllegalArgumentCorrectFormat() {
    assertThrowsExactly(IllegalArgumentException.class, () -> {
      Time.parseTime("ab:cd");
    });
  }

  @Test
  void testParseIllegalArgumentEmpty() {
    assertThrowsExactly(IllegalArgumentException.class, () -> {
      Time.parseTime("");
    });
  }

  @Test
  void testParseIllegalArgumentHour() {
    assertThrowsExactly(IllegalArgumentException.class, () -> {
      Time.parseTime("122:00");
    });
  }

  @Test
  void testParseIllegalArgumentMinute() {
    assertThrowsExactly(IllegalArgumentException.class, () -> {
      Time.parseTime("12:000");
    });
  }

  @Test
  void testParseIllegalArgumentExceedHour() {
    assertThrowsExactly(IllegalArgumentException.class, () -> {
      Time.parseTime("48:00");
    });
  }

  @Test
  void testParseIllegalArgumentExceedMinute() {
    assertThrowsExactly(IllegalArgumentException.class, () -> {
      Time.parseTime("01:60");
    });
  }

  @Test
  void testParseIllegalFormat() {
    assertThrowsExactly(IllegalArgumentException.class, () -> {
      Time.parseTime("12-00");
    });
  }
}