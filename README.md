# Train Dispatch System
A train dispatch system that can add, remove, and modify departures from a station.

## Installation

1. Download the project with `$ git clone --recursive https://gitlab.stud.idi.ntnu.no/markumad/idatx1003-mappeoppgave`
2. Navigate to the traindispatchsystem folder `$ cd traindispatchsystem`
3. Run `$ mvn clean compile package` to create a runnable .jar file
4. Navigate to the target folder `$ cd target`
5. Run the .jar file with `$ java -jar traindispatchsystem-1.0.0-SNAPSHOT.jar`

Alternatively, the program can be started by running the "Main" class.

## Usage
The program starts with a menu that allows the user to choose between the following options:

0: Quit

- Quits the program

1: Show time table

- Shows the time table

2: Set current time

- Manually set the current time
- The time set can not be before the current time and not more than 23:59

3: Add train

- Add a train to the time table
- The user is asked to enter:
    - Train number (unique)
    - Line
    - Destination
    - Departure time
    - Track number
    - Delay
- If the departure time is before the current time, it is automatically set to be the next day

4: Search
- Search for a train by train number
    - The user is asked to enter a train number
    - If a train is found, the following actions can be performed:
        - Return to the main menu
        - Show train
        - Change track
        - Change delay
        - Change destination
        - Delete train

- Search for a train by destination
    - The user is asked to enter a destination
    - If a train is found, the resulting time table is shown

## Features
### Train dispatch system
A train dispatch system that can add, remove, and modify departures. Supports one station, and departures within 24 hours. Has a built in clock that is updated manually.

### Menu system
Fully customizable, blazing fast menu system with support for submenus and dynamic menu items.

### Dynamic parser
This project features a dynamic parser that can parse from a String into any class with a suitable parse function.

### Time handling
Custom time handling within 48 hours, with support for adding and comparing times. 
Includes a Time parser that can parse from a String into a Time object.
